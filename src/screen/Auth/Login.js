import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import {
  TextRegular,
  TextMedium,
  TextBold,
  InputText,
} from '../../component/global';
import {Colors} from '../../styles';
import ModalBottom from '../../component/modal/ModalBottom';
import ModalCenter from '../../component/modal/ModalCenter';
import {NumberFormatter, validateEmail} from '../../utils';

import LoginComponent from '../../component/section/Auth/LoginComponent';

const Login = ({navigation, route}) => {
  const [email, setEmail] = useState('');
  const [telpon, setTelpon] = useState('');
  const [password, setPassword] = useState('');
  const [passwordshown, setpasswordshown] = useState(false);
  const [hidePassword, setHidePassword] = useState(true);

  const [modalBottom, setModalBottom] = useState(false);
  const [modalCenter, setModalCenter] = useState(false);

  const onLogin = () => {
    const contohEmail = 'codemasters@gmail.com';
    console.log('email: ', email);
    console.log('password: ', password);
    if (!validateEmail(email)) {
      console.log('email format is wrong!');
    } else {
      console.log('email format is correct!');
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <LoginComponent
        navigation={navigation}
        email={email}
        setEmail={text => setEmail(text)}
        password={password}
        setPassword={text => setPassword(text)}
        hidePassword={hidePassword}
        setShowPassword={value => setHidePassword(value)}
        onPressLogin={() => onLogin()}
      />
      {/* GLOBAL FUNCTION ===================================================*/}
      {/* <View style={{flex: 1, backgroundColor: 'white'}}> */}
      <TextRegular 
        text={`${NumberFormatter(100000000, 'Rp. ')}`} 
        size={16}
        color={Colors.BLACK}
        style={{
          marginTop: 20,
          marginLeft: 20
        }}
        />
      {/* </View> */}

      {/* BUTTON =============================================================*/}
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <TouchableOpacity
          style={styles.btnShowModalBottom}
          onPress={() => onLogin()}>
          <TextBold text="OnLogin Check" size={16} color="white" />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setModalBottom(true)}
          style={[
            styles.btnShowModalBottom,
            {backgroundColor: Colors.AQUA},
          ]}>
          <TextBold text="Modal Bottom" size={16} color="black" />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setModalCenter(true)}
          style={[
            styles.btnShowModalBottom,
            {backgroundColor: Colors.DEEPORANGE},
          ]}>
          <TextBold text="Modal Center" size={16} color="white" />
        </TouchableOpacity>
      </View>

      {/* MODAL CENTER =========================================================*/}
      <ModalCenter
        show={modalCenter}
        onClose={() => setModalCenter(false)}
        title={'Modal Center'}>
        <TextRegular text="Text Reguler" color={Colors.BLACK} />
        <TextMedium text={'Text Medium'} color={Colors.AQUA} />
        <TextBold text={'Text Bold'} color={Colors.PRIMARY} />
      </ModalCenter>

      {/* MODAL BOTTOM =========================================================*/}
      <ModalBottom
        show={modalBottom}
        onClose={() => setModalBottom(false)}
        title="Modal Bottom">
        <View>
          <TextRegular text="Text Reguler" color={Colors.BLACK} />
          <TextMedium text={'Text Medium'} color={Colors.AQUA} />
          <TextBold text={'Text Bold'} color={Colors.PRIMARY} />
        </View>
      </ModalBottom>
      {/* TEXT */}
      {/* <TextRegular text="Text Reguler" color={Colors.BLACK} />
      <TextMedium text={'Text Medium'} color={Colors.AQUA} />
      <TextBold text={'Text Bold'} color={Colors.PRIMARY} /> */}
      {/* INPUT TEXT */}
      {/* <InputText
        style={{width: '90%', alignSelf: 'center', marginTop: 20}}
        placeholderText="Masukkan Email"
        keyboardType="email-address"
        onChangeText={text => {
          setEmail(text);
        }}
        value={email}
      />
      <InputText
        style={{width: '90%', alignSelf: 'center', marginTop: 20}}
        placeholderText="Masukkan Nomor Telpon"
        keyboardType="number-pad"
        onChangeText={text => {
          setTelpon(text);
        }}
        value={telpon}
      />
      <InputText
        style={{width: '90%', alignSelf: 'center', marginVertical: 20}}
        placeholderText="Masukkan Password"
        isPassword={true}
        onChangeText={text => {
          setPassword(text);
        }}
        value={password}
        showPassword={passwordshown}
        setShowPassword={value => setpasswordshown(value)}
      /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  btnShowModalBottom: {
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: Colors.PRIMARY,
    alignSelf: 'center',
    marginTop: 20,
  },
});

export default Login;