import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import Routing from './src/navigation/Routing';

const App = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar barStyle={'light-content'} />
      <Routing />
    </SafeAreaView>
  );
};

export default App;

